import bcrypt from 'bcrypt'

const SALT_WORK_FACTOR = 10

export function comparePasswords(candidatePassword: string, hashedPassword: string) {
  return bcrypt.compare(candidatePassword, hashedPassword)
}

export async function hashPassword(password: string) {
  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR)

  return await bcrypt.hash(password, salt)
}
